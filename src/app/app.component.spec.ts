import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  MatCardModule,
  MatIconModule,
  MatInputModule,
  MatProgressSpinnerModule,
  MatSlideToggleModule,
  MatTableModule,
  MatToolbarModule,
} from '@angular/material';
import { of } from 'rxjs';

import { AppComponent } from './app.component';
import { TickerComponent } from './components/ticker/ticker.component';
import { WalletComponent } from './components/wallet/wallet.component';
import { DummyService } from './services/dummy/dummy.service';
import { GlobalService } from './services/global/global.service';

const globalService = jasmine.createSpyObj('GlobalService',
  ['getDefaultCrypto',
    'getDefaultCurrency',
    'getDefaultTickerTime'
  ]);
const dummyService = jasmine.createSpyObj('DummyService', ['useDummies']);

describe('AppComponent', () => {
  let fixture;
  let component;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MatTableModule, MatToolbarModule,
        MatProgressSpinnerModule, MatSlideToggleModule,
        FormsModule, ReactiveFormsModule, MatCardModule,
        MatIconModule, MatInputModule, HttpClientTestingModule,
      ],
      providers: [
        AppComponent,
        { provide: GlobalService, useValue: globalService },
        { provide: DummyService, useValue: dummyService }
      ],
      declarations: [
        AppComponent, TickerComponent, WalletComponent
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(AppComponent);
    component = fixture.debugElement.componentInstance;
    dummyService.useDummies.and.returnValue(of(true));
  }));

  // fit('should create the app', (done: DoneFn) => {
  //   expect(true).toBe(true);
  //   done();
  // });

});
