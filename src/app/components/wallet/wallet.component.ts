import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { GlobalService } from 'src/app/services/global/global.service';
import { TickerService } from 'src/app/services/ticker/ticker.service';
import { WalletService } from 'src/app/services/wallet/wallet.service';

import { IWallet } from '../../models/iwallet';

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}


@Component({
  selector: 'app-wallet',
  templateUrl: './wallet.component.html',
  styleUrls: ['./wallet.component.css']
})
export class WalletComponent implements OnInit {

  public displayedColumns = ['name', 'amount', 'ticker'];
  public wallet$: Observable<IWallet>;
  public tickers$ = [];

  public matcher = new MyErrorStateMatcher();

  public publicAddress = new FormControl('', [
    Validators.required
  ]);

  public isLoading$: Observable<boolean> = this.walletService.isLoading();

  constructor(private walletService: WalletService,
    private tickerService: TickerService,
    private globalService: GlobalService) {
  }

  ngOnInit() {
  }

  public keyDown(event) {
    if (event.keyCode !== 13) { return; }

    this.publicAddress.value.trim();

    this.wallet$ = this.getWallet();

    this.wallet$
      .subscribe(wallet => {
        wallet.balances.forEach(coin => {
          this.tickers$[coin.name] = this.tickerService.getTickerFor(coin.name, this.globalService.getDefaultCurrency());
        });
      });
  }

  public clearWallet() {
    this.wallet$ = null;
    this.walletService.clearWallet();
  }

  private getWallet(): Observable<IWallet> {
    return this.walletService.getWallet(this.publicAddress.value, '');
  }


}
