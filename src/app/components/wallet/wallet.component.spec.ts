import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  MatCardModule,
  MatIconModule,
  MatInputModule,
  MatProgressSpinnerModule,
  MatSlideToggleModule,
  MatTableModule,
  MatToolbarModule,
} from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { of } from 'rxjs';
import { ICoin } from 'src/app/models/icoin';
import { IWallet } from 'src/app/models/iwallet';
import { ITicker } from 'src/app/models/iticker';
import { DummyService } from 'src/app/services/dummy/dummy.service';
import { GlobalService } from 'src/app/services/global/global.service';
import { TickerService } from 'src/app/services/ticker/ticker.service';
import { WalletService } from 'src/app/services/wallet/wallet.service';

import { WalletComponent } from './wallet.component';
import { ElementRef } from '@angular/core';

const globalService = jasmine.createSpyObj('GlobalService', ['getDefaultCrypto', 'getDefaultCurrency']);
const tickerService = jasmine.createSpyObj('TickerService', ['getTickerFor']);
const dummyService = jasmine.createSpyObj('DummyService', ['useDummies']);
const walletService = jasmine.createSpyObj('WalletService', ['getWallet', 'clearWallet', 'isLoading']);

const triggerEnterKeyPress = function (element) {
  const e = new KeyboardEvent('keydown', {
    bubbles: true,
    cancelable: true,
    shiftKey: true
  });
  Object.defineProperty(e, 'keyCode', { 'value': 13 });
  element.dispatchEvent(e);
};

const stubWallet = {
  balances: [{
    name: 'NEO',
    valid: 15,
    total: 15
  },
  {
    name: `GAS`,
    valid: 16,
    total: 16
  }],
  address: ''
} as IWallet;

const stubTicker = {
  askPrice: 16.16300000,
  askQty: 27.42300000,
  bidPrice: 16.14800000,
  bidQty: 7.42300000,
  closeTime: 1539274056209,
  count: 36124,
  firstId: 13239330,
  highPrice: 18.38500000,
  lastId: 13275453,
  lastPrice: 16.14700000,
  lastQty: 8.07600000,
  lowPrice: 15.59200000,
  openPrice: 18.11600000,
  openTime: 1539187656209,
  prevClosePrice: 18.11600000,
  priceChange: -1.96900000,
  priceChangePercent: -10.869,
  quoteVolume: 14918551.12709300,
  symbol: 'NEOUSDT',
  volume: 885660.32500000,
  weightedAvgPrice: 16.84455169,
} as ITicker;

describe('WalletComponent', () => {
  let component: WalletComponent;
  let fixture: ComponentFixture<WalletComponent>;
  let htmlElement: HTMLElement;
  let inputElement: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MatTableModule, MatToolbarModule,
        MatProgressSpinnerModule, MatSlideToggleModule,
        FormsModule, ReactiveFormsModule, MatCardModule,
        MatIconModule, MatInputModule, HttpClientTestingModule,
        BrowserAnimationsModule
      ],
      providers: [
        WalletComponent,
        { provide: GlobalService, useValue: globalService },
        { provide: TickerService, useValue: tickerService },
        { provide: DummyService, useValue: dummyService },
        { provide: WalletService, useValue: walletService }
      ],
      declarations: [WalletComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WalletComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    walletService.getWallet.and.returnValue(of(stubWallet));
    walletService.isLoading.and.returnValue(of(false));

    tickerService.getTickerFor.and.returnValue(of(stubTicker));
    globalService.getDefaultCurrency.and.returnValue('USDT');

    htmlElement = fixture.nativeElement;
    inputElement = htmlElement.querySelector('.full-length input');
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have a wallet with matching tickers after enter keypress', (done: DoneFn) => {
    component.publicAddress.setValue('NEW PUBLIC ADDRESS');

    triggerEnterKeyPress(inputElement);

    component.wallet$.subscribe(wallet => {
      wallet.balances.forEach(
        (coin: ICoin) => expect(component.tickers$[coin.name]).toBeDefined()
      );
      done();
    });
  });

  it('should be able to clear wallet', (done: DoneFn) => {
    component.publicAddress.setValue('NEW PUBLIC ADDRESS');

    triggerEnterKeyPress(inputElement);
    component.wallet$.subscribe(wallet => {
      wallet.balances.forEach(
        (coin: ICoin) => expect(component.tickers$[coin.name]).toBeDefined()
      );
      component.clearWallet();
      expect(component.wallet$).toBeNull();
      done();
    });

  });
});
