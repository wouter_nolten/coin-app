import { Component, OnInit } from '@angular/core';
import { GlobalService } from 'src/app/services/global/global.service';

import { ITicker } from '../../models/iticker';
import { TickerService } from '../../services/ticker/ticker.service';

@Component({
  selector: 'app-ticker',
  templateUrl: './ticker.component.html',
  styleUrls: ['./ticker.component.css']
})
export class TickerComponent implements OnInit {
  public ticker: ITicker;
  public crypto = this.globalService.getDefaultCrypto();

  constructor(private tickerService: TickerService,
    private globalService: GlobalService) {
  }

  ngOnInit() {
    this.tickerService.getTickerFor(this.crypto, this.globalService.getDefaultCurrency()).subscribe(ticker => this.ticker = ticker);
  }
}
