import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatCardModule, MatProgressSpinnerModule } from '@angular/material';
import { GlobalService } from 'src/app/services/global/global.service';

import { TickerComponent } from './ticker.component';

const globalService = jasmine.createSpyObj('GlobalService', ['getDefaultCurrency', 'getDefaultTickerTime', 'getDefaultCrypto']);

describe('TickerComponent', () => {
  let component: TickerComponent;
  let fixture: ComponentFixture<TickerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MatProgressSpinnerModule, MatCardModule,
        HttpClientModule
      ],
      providers: [
        TickerComponent,
        { provide: GlobalService, useValue: globalService }
      ],
      declarations: [TickerComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {

    globalService.getDefaultCurrency.and.returnValue('USDT');
    globalService.getDefaultTickerTime.and.returnValue(1000);
    globalService.getDefaultCrypto.and.returnValue('NEO');

    fixture = TestBed.createComponent(TickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
