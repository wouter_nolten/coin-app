import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatCardModule, MatFormFieldModule, MatProgressSpinnerModule, MatSelectModule } from '@angular/material';
import * as moment from 'moment';
import { DcaService } from 'src/app/services/dca/dca.service';
import { InlineEditComponent } from 'src/app/shared/inline-edit/inline-edit.component';

import { DcaComponent } from './dca.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

const stubDate = moment().add('1', 'days');
const dcaService = jasmine.createSpyObj('DcaService',
  ['getDCAAmount',
    'getNextDCADate',
    'getCurrentDCAFrequency',
    'getDCAFrequencies']);
const stubCurrentDCAFrequency = 'day';
const stubDCAFrequencies: string[] = [
  'day',
  'week',
  'month',
  'year'
];


describe('DcaComponent', () => {
  let dcaComponent: DcaComponent;
  let fixture: ComponentFixture<DcaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        BrowserAnimationsModule,
        FormsModule, ReactiveFormsModule,
        MatProgressSpinnerModule, MatCardModule,
        HttpClientModule, MatFormFieldModule, MatSelectModule
      ],
      providers: [
        { provide: DcaService, useValue: dcaService }
      ],
      declarations: [DcaComponent, InlineEditComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DcaComponent);
    dcaComponent = fixture.componentInstance;
    fixture.detectChanges();

    dcaService.getDCAAmount.and.returnValue(50);
    dcaService.getNextDCADate.and.returnValue(stubDate);
    dcaService.getCurrentDCAFrequency.and.returnValue(stubCurrentDCAFrequency);
    dcaService.getDCAFrequencies.and.returnValue(stubDCAFrequencies);
  });

  it('should create', () => {
    expect(dcaComponent).toBeTruthy();
  });

  it('should have a field which displays next DCA', () => {
    const nextDCADate = dcaComponent.getNextDCADate();
    expect(nextDCADate).toEqual(stubDate);
  });

  it('should have a field which displays next DCA', () => {
    const currentFrequency = dcaComponent.getCurrentDCAFrequency();
    expect(currentFrequency).toEqual(stubCurrentDCAFrequency);
  });

  it('should have a field which displays next DCA', () => {
    const dcaFrequencies = dcaComponent.getDCAFrequencies();
    expect(dcaFrequencies).toEqual(stubDCAFrequencies);
  });
});
