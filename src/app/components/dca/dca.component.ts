import { Component, OnInit, LOCALE_ID } from '@angular/core';
import { DcaService } from 'src/app/services/dca/dca.service';
import { IEditable } from 'src/app/shared/IEditable';
import { CurrencyPipe, DatePipe } from '@angular/common';
import * as moment from 'moment';
import { FormControl } from '@angular/forms';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-dca',
  templateUrl: './dca.component.html',
  styleUrls: ['./dca.component.css']
})
export class DcaComponent implements OnInit {

  public readonly editable = this.getEditableCommand();
  public selectedDCAFrequency = new FormControl(this.getCurrentDCAFrequency());

  constructor(private dcaService: DcaService) { }

  ngOnInit() {
    this.selectedDCAFrequency.valueChanges
      .subscribe((newFrequency) => this.dcaService.updateDCAFrequency(newFrequency));
  }

  private getEditableCommand(): IEditable {
    return {
      type: 'number',
      value: this.dcaService.getDCAAmount(),
      id: 1,
      transformPipe: 'currency'
    } as IEditable;
  }

  public getCurrentDCAFrequency(): string {
    return this.dcaService.getCurrentDCAFrequency();
  }

  public getNextDCADate(): moment.Moment {
    return this.dcaService.getNextDCADate();
  }

  public getDCAFrequencies(): string[] {
    return this.dcaService.getDCAFrequencies();
  }
}
