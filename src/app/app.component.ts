import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { startWith } from 'rxjs/operators';

import { DummyService } from './services/dummy/dummy.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  public useDummies = new FormControl(false);

  constructor(private dummyService: DummyService) { }

  ngOnInit() {
    this.useDummies.valueChanges
      .pipe(
        startWith(this.useDummies.value)
      ).subscribe(useDummies => this.dummyService.setDummies(useDummies));
  }
}
