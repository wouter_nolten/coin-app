import { PipeTransform } from '@angular/core';

export interface IEditable {
  type: string;
  placeholder?: string;
  value: any;
  id: number;
  transformPipe?: string;
}

// interface IEditablePipe {
//   pipe: PipeTransform;
//   args?: any;
// }
