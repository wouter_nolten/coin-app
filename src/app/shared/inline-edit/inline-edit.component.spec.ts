import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material';
import { take } from 'rxjs/operators';

import { IEditable } from '../IEditable';
import { InlineEditComponent } from './inline-edit.component';

const triggerEnterKeyPress = function (element) {
  const e = new KeyboardEvent('keyup', {
    bubbles: true,
    cancelable: true,
    shiftKey: true
  });
  Object.defineProperty(e, 'keyCode', { 'value': 13 });
  element.dispatchEvent(e);
};

const selectElement = (topHTMLElement: HTMLElement, htmlType: string): HTMLElement =>
  topHTMLElement.querySelector(htmlType);

const editable = {
  type: 'number',
  value: 50,
  id: 1,
  transformPipe: 'currency'
} as IEditable;

export function createFakeEvent(type: string) {
  const event = document.createEvent('Event');
  event.initEvent(type, true, true);
  return event;
}

export function dispatchFakeEvent(node: Node | Window, type: string) {
  node.dispatchEvent(createFakeEvent(type));
}

describe('InlineEditComponent', () => {
  let component: InlineEditComponent;
  let fixture: ComponentFixture<InlineEditComponent>;
  let htmlElement: HTMLElement;
  let buttonElement: HTMLElement;
  const HTMLElements = [];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [MatInputModule, FormsModule],
      declarations: [InlineEditComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InlineEditComponent);
    component = fixture.componentInstance;

    component.editable = editable;
    fixture.detectChanges();

    document.getElementById = jasmine.createSpy('HTML Element').and.callFake((ID: string) => {
      if (!HTMLElements[ID]) {
        const newElement = document.createElement('div');
        HTMLElements[ID] = newElement;
      }
      return HTMLElements[ID];
    });

    htmlElement = fixture.nativeElement;
    buttonElement = selectElement(htmlElement, 'button');
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should change to input on button click', (done: DoneFn) => {
    let isEditing;
    component.editing$.pipe(take(1)).subscribe(editing => isEditing = editing);
    buttonElement.click();
    fixture.detectChanges();

    component.editing$.pipe(take(1)).subscribe(editing => {
      expect(isEditing).toEqual(false);
      expect(editing).toEqual(true);
      expect(selectElement(htmlElement, 'input')).not.toBeNull();
      expect(selectElement(htmlElement, 'button')).toBeNull();
      done();
    });
  });

  it('should change back to button on enter press', (done: DoneFn) => {
    buttonElement.click();
    fixture.detectChanges();
    let isEditing;
    component.editing$.pipe(take(1)).subscribe(editing => isEditing = editing);

    triggerEnterKeyPress(selectElement(htmlElement, 'input'));
    fixture.detectChanges();

    component.editing$.pipe(take(1)).subscribe(editing => {
      expect(isEditing).toEqual(true);
      expect(editing).toEqual(false);
      expect(selectElement(htmlElement, 'input')).toBeNull();
      expect(selectElement(htmlElement, 'button')).not.toBeNull();
      done();
    });
  });
});
