import { Component, Input, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { skip } from 'rxjs/operators';

import { IEditable } from '../IEditable';

@Component({
  selector: 'app-inline-edit',
  templateUrl: './inline-edit.component.html',
  styleUrls: ['./inline-edit.component.css']
})
export class InlineEditComponent implements OnInit {

  private readonly ENTER_KEY = 13;
  private readonly ESCAPE_KEY = 27;

  public editing$ = new BehaviorSubject<boolean>(false);
  @Input() public editable: IEditable;

  constructor() { }
  ngOnInit() {
    this.editing$.pipe(skip(1)).subscribe(
      (newEditing) => {
        if (newEditing) {
          setTimeout(() => {
            const inputElement: HTMLElement = document.querySelector('.' + this.getHTMLID('input'));
            inputElement.focus();
          });
        }
      });
  }

  public pressedKey(key: KeyboardEvent) {
    switch (key.keyCode) {
      case this.ENTER_KEY:
      case this.ESCAPE_KEY:
        this.setEditing(false);
    }
  }

  public setEditing(newEditing: boolean) {
    this.editing$.next(newEditing);
  }

  public getHTMLID(htmlType: string) {
    return htmlType + '_inline_edit_' + this.editable.id;
  }
}
