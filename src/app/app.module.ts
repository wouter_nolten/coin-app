import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  ErrorStateMatcher,
  MatButtonModule,
  MatCardModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatProgressSpinnerModule,
  MatSlideToggleModule,
  MatTableModule,
  MatToolbarModule,
  ShowOnDirtyErrorStateMatcher,
  MatSelectModule,
} from '@angular/material';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { DcaComponent } from './components/dca/dca.component';
import { TickerComponent } from './components/ticker/ticker.component';
import { WalletComponent } from './components/wallet/wallet.component';
import { BinanceService } from './services/binance/binance.service';
import { DcaService } from './services/dca/dca.service';
import { DummyService } from './services/dummy/dummy.service';
import { GlobalService } from './services/global/global.service';
import { TickerService } from './services/ticker/ticker.service';
import { WalletService } from './services/wallet/wallet.service';
import { InlineEditComponent } from './shared/inline-edit/inline-edit.component';

@NgModule({
  declarations: [
    AppComponent,
    TickerComponent,
    WalletComponent,
    DcaComponent,
    InlineEditComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatButtonModule,
    MatCardModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatProgressSpinnerModule,
    MatSelectModule,
    MatSlideToggleModule,
    MatToolbarModule,
    MatTableModule,
  ],
  providers: [BinanceService,
    WalletService,
    TickerService,
    DummyService,
    GlobalService,
    DcaService,
    { provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
