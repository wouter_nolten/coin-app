export interface ICoin {
    assetId: string;
    assetType: string;
    frozen: string;
    marketSign: string;
    name: string;
    total: number;
    valid: number;
}
