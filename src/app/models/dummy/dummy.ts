import { ITicker } from '../iticker';
import { IWallet } from '../iwallet';

export class Dummy {
    public readonly DUMMY_TICKER_NEOUSDT = {
        askPrice: 16.16300000,
        askQty: 27.42300000,
        bidPrice: 16.14800000,
        bidQty: 7.42300000,
        closeTime: 1539274056209,
        count: 36124,
        firstId: 13239330,
        highPrice: 18.38500000,
        lastId: 13275453,
        lastPrice: 16.14700000,
        lastQty: 8.07600000,
        lowPrice: 15.59200000,
        openPrice: 18.11600000,
        openTime: 1539187656209,
        prevClosePrice: 18.11600000,
        priceChange: -1.96900000,
        priceChangePercent: -10.869,
        quoteVolume: 14918551.12709300,
        symbol: `NEOUSDT`,
        volume: 885660.32500000,
        weightedAvgPrice: 16.84455169,
    } as ITicker;

    public readonly DUMMY_TICKER_GASBTC = {
        askPrice: 0.00082500,
        askQty: 9.07000000,
        bidPrice: 0.00082200,
        bidQty: 146.52000000,
        closeTime: 1539274388421,
        count: 7725,
        firstId: 3583613,
        highPrice: 0.00089400
        , lastId: 3591337
        , lastPrice: 0.00082200
        , lastQty: 0.03000000
        , lowPrice: 0.00080000
        , openPrice: 0.00088800
        , openTime: 1539187988421
        , prevClosePrice: 0.00088800
        , priceChange: -0.00006600
        , priceChangePercent: -7.432
        , quoteVolume: 96.55040782
        , symbol: `GASBTC`
        , volume: 114701.08000000
        , weightedAvgPrice: 0.00084176
    } as ITicker;

    public readonly DUMMY_TICKER_BTCUSDT =
        {
            askPrice: 6305.80000000,
            askQty: 1.01322400,
            bidPrice: 6305.00000000,
            bidQty: 0.10000000,
            closeTime: 1539274288930,
            count: 218602,
            firstId: 74080261,
            highPrice: 6646.20000000,
            lastId: 74298862,
            lastPrice: 6305.00000000,
            lastQty: 0.21368300,
            lowPrice: 6219.17000000,
            openPrice: 6590.08000000,
            openTime: 1539187888930,
            prevClosePrice: 6590.08000000,
            priceChange: -285.08000000,
            priceChangePercent: -4.326,
            quoteVolume: 262469671.19229922,
            symbol: `BTCUSDT`,
            volume: 41273.88949500,
            weightedAvgPrice: 6359.21824678,
        } as ITicker;

    public readonly DUMMY_WALLET = {
        balances: [{
            name: 'NEO',
            valid: 15,
            total: 15
        },
        {
            name: 'GAS',
            valid: 16,
            total: 16
        }],
        address: ''
    } as IWallet;

}
