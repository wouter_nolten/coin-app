import { ICoin } from './icoin';
export interface IWallet {
  address: string;
  balances: ICoin[];
}
