import { HttpClientModule } from '@angular/common/http';
import { inject, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';

import { ITicker } from '../../models/iticker';
import { BinanceService } from '../binance/binance.service';
import { GlobalService } from '../global/global.service';
import { TickerService } from './ticker.service';

const binanceService = jasmine.createSpyObj('BinanceService', ['getTicker']);
const globalService = jasmine.createSpyObj('GlobalService', ['getDefaultCurrency', 'getDefaultTickerTime']);
const ticker = {
  askPrice: 16.16300000,
  askQty: 27.42300000,
  bidPrice: 16.14800000,
  bidQty: 7.42300000,
  closeTime: 1539274056209,
  count: 36124,
  firstId: 13239330,
  highPrice: 18.38500000,
  lastId: 13275453,
  lastPrice: 16.14700000,
  lastQty: 8.07600000,
  lowPrice: 15.59200000,
  openPrice: 18.11600000,
  openTime: 1539187656209,
  prevClosePrice: 18.11600000,
  priceChange: -1.96900000,
  priceChangePercent: -10.869,
  quoteVolume: 14918551.12709300,
  symbol: 'NEOUSDT',
  volume: 885660.32500000,
  weightedAvgPrice: 16.84455169,
} as ITicker;

const invalidTicker = {
  askPrice: 0,
  askQty: 0,
  bidPrice: 0,
  bidQty: 0,
  closeTime: 0,
  count: 0,
  firstId: 0,
  highPrice: 0,
  lastId: 0,
  lastPrice: 0,
  lastQty: 0,
  lowPrice: 0,
  openPrice: 0,
  openTime: 0,
  prevClosePrice: 0,
  priceChange: 0,
  priceChangePercent: 0,
  quoteVolume: 0,
  symbol: 'INVALIDTICKER',
  volume: 0,
  weightedAvgPrice: 0,
} as ITicker;

describe('TickerService', () => {
  let tickerService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [TickerService,
        { provide: BinanceService, useValue: binanceService },
        { provide: GlobalService, useValue: globalService }
      ]
    });

    globalService.getDefaultCurrency.and.returnValue('USDT');
    globalService.getDefaultTickerTime.and.returnValue(1000);
    tickerService = TestBed.get(TickerService);
  });

  it('should be created', inject([TickerService], (service: TickerService) => {
    expect(service).toBeTruthy();
  }));

  it('should return a valid USDT ticker for a valid USDT token', (done: DoneFn) => {
    binanceService.getTicker.and.returnValue(of(ticker));

    tickerService.getTickerFor('NEO', 'USDT').subscribe(value => {
      expect(value).toEqual(ticker);
      done();
    });
  });

  it('should return an EMPTY observable when no valid ticker is being called', (done: DoneFn) => {
    binanceService.getTicker.and.returnValue(of(invalidTicker));

    tickerService.getTickerFor('INVALID', 'TOKEN').subscribe(testInvalidTicker => {
      expect(testInvalidTicker).toEqual(invalidTicker);
      done();
    });
  });

});
