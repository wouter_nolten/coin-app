import { Injectable } from '@angular/core';
import { EMPTY, Observable, of, Subject, timer, zip } from 'rxjs';
import { catchError, exhaustMap, switchMap, map } from 'rxjs/operators';

import { ITicker } from '../../models/iticker';
import { BinanceService } from '../binance/binance.service';
import { GlobalService } from '../global/global.service';

@Injectable({
  providedIn: 'root'
})
export class TickerService {

  private refresh$ = new Subject<void>();
  private tickerList = [];
  private BTCUSDTTicker$: Observable<ITicker>;

  constructor(private binanceService: BinanceService,
    private globalService: GlobalService) {
    this.BTCUSDTTicker$ = this.getTickerFor('BTC', 'USDT');
    this.startTickerTimer(this.globalService.getDefaultTickerTime());
  }

  public getTickerFor(symbol: string, currency: string): Observable<ITicker> {
    const symbolCurrency = symbol.toUpperCase() + currency.toUpperCase();

    // console.log('getting ticker for ', symbol, currency);
    if (this.tickerList[symbolCurrency]) {
      return this.tickerList[symbolCurrency];
    }

    if (currency === this.globalService.getDefaultCurrency()) {
      this.tickerList[symbolCurrency] = this.refresh$.pipe(
        exhaustMap(() => this.binanceService.getTicker(symbolCurrency)),
        catchError(err => {
          if (err.error.code === -1121) {
            return this.getTickerFor(symbol, 'BTC');
          }
          console.log('Error de los muertos: ', err); // todo: error handling
          return of(this.globalService.getInvalidTicker());
        })
      );
      return this.tickerList[symbolCurrency];
    }

    const ticker = this.refresh$.pipe(
      exhaustMap(() => this.binanceService.getTicker(symbolCurrency)),
      catchError(err => { console.log('Err', err); return EMPTY; })
    );

    this.tickerList[symbolCurrency] = this.getUSDTTicker(this.BTCUSDTTicker$, ticker);
    return this.tickerList[symbolCurrency];
  }

  private getUSDTTicker(
    BTCUSDTTicker$: Observable<ITicker>,
    symbolBTCTicker$: Observable<ITicker>): Observable<ITicker> {
    return zip(BTCUSDTTicker$, symbolBTCTicker$).pipe(
      map(([BTCUSDT, symbolBTC]: [ITicker, ITicker]) => this.convertToUSDT(BTCUSDT.lastPrice, symbolBTC))
    );
  }

  private startTickerTimer(milliSeconds: number) {
    timer(0, milliSeconds).pipe(
      exhaustMap(() => of(this.refreshTicker()))
    ).subscribe();
  }

  private convertToUSDT(lastBTCPrice: number, ticker: ITicker): ITicker {
    const askPrice = ticker.askPrice * lastBTCPrice;
    const weightedAvgPrice = ticker.weightedAvgPrice * lastBTCPrice;
    const bidPrice = ticker.bidPrice * lastBTCPrice;
    const lowPrice = ticker.lowPrice * lastBTCPrice;
    const highPrice = ticker.highPrice * lastBTCPrice;

    return {
      ...ticker,
      askPrice: askPrice,
      weightedAvgPrice: weightedAvgPrice,
      bidPrice: bidPrice,
      lowPrice: lowPrice,
      highPrice: highPrice,
    } as ITicker;
  }

  private refreshTicker() {
    this.refresh$.next(null);
  }
}
