import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { inject, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { ITicker } from 'src/app/models/iticker';

import { DummyService } from '../dummy/dummy.service';
import { TimerService } from '../timerservice/timer.service';
import { BinanceService } from './binance.service';

const dummyService = jasmine.createSpyObj('DummyService', ['useDummies', 'getDummyFor']);
const timerService = jasmine.createSpyObj('TimerService', ['getTimer']);
const stubTicker = {
  askPrice: 16,
  askQty: 27,
  bidPrice: 16,
  bidQty: 7,
  closeTime: 1539274056209,
  count: 36124,
  firstId: 13239330,
  highPrice: 18,
  lastId: 13275453,
  lastPrice: 16,
  lastQty: 8,
  lowPrice: 15,
  openPrice: 18,
  openTime: 1539187656209,
  prevClosePrice: 18,
  priceChange: -1,
  priceChangePercent: -10,
  quoteVolume: 14918551,
  symbol: 'NEOUSDT',
  volume: 885660,
  weightedAvgPrice: 16,
} as ITicker;

const dummyTicker = {
  askPrice: 20,
  askQty: 28,
  bidPrice: 17,
  bidQty: 8,
  closeTime: 1539274056209,
  count: 36125,
  firstId: 13239330,
  highPrice: 19,
  lastId: 13275453,
  lastPrice: 17,
  lastQty: 9,
  lowPrice: 16,
  openPrice: 19,
  openTime: 1539187656209,
  prevClosePrice: 19,
  priceChange: -2,
  priceChangePercent: -11,
  quoteVolume: 14918552,
  symbol: 'NEOUSDT',
  volume: 885661,
  weightedAvgPrice: 17,
} as ITicker;


describe('BinanceService', () => {
  let binanceService: BinanceService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [BinanceService,
        { provide: DummyService, useValue: dummyService },
        { provide: TimerService, useValue: timerService }
      ]
    });

    // Inject the http, test controller, and service-under-test
    // as they will be referenced by each test.
    httpTestingController = TestBed.get(HttpTestingController);

    dummyService.useDummies.and.returnValue(of(false));
    dummyService.getDummyFor.and.returnValue(of(dummyTicker));
    timerService.getTimer.and.returnValue(of(1)); // only call refresh once.

    binanceService = TestBed.get(BinanceService);
  });

  it('should be created', inject([BinanceService], (service: BinanceService) => {
    expect(service).toBeTruthy();
  }));

  it('should have an APIUrl', () => {
    expect(binanceService.getAPIURL()).not.toBeNull();
    expect(binanceService.getAPIURL()).not.toEqual('');
    expect(binanceService.getAPIURL()).toContain('http');
  });

  it('should get ticker with dummies turned off', (done: DoneFn) => {
    const symbol = 'NEOUSDT';
    binanceService.getTicker(symbol).subscribe(ticker => {
      expect(ticker).toBeDefined();
      expect(ticker.symbol).toEqual(symbol);
      expect(ticker.askPrice).toEqual(stubTicker.askPrice);
      done();
    });

    dummyService.useDummies.and.returnValue(of(false));
    binanceService.refresh();

    const req = httpTestingController.expectOne(binanceService.getAPIURL() + '/ticker/24hr?symbol=' + symbol);
    expect(req.request.method).toEqual('GET');

    // Respond with the mock wallet
    req.flush(stubTicker);
  });

  it('should get wallet with dummies turned on', (done: DoneFn) => {
    const symbol = 'NEOUSDT';
    dummyService.useDummies.and.returnValue(of(true));

    binanceService.getTicker(symbol).subscribe(ticker => {
      expect(ticker).toBeDefined();
      expect(ticker.symbol).toEqual(symbol);
      expect(ticker.askPrice).toEqual(dummyTicker.askPrice);
      done();
    });

    binanceService.refresh();
  });
});
