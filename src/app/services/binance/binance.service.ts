import { HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { combineLatest, Observable, of, Subject } from 'rxjs';
import { exhaustMap, filter, map, switchMap } from 'rxjs/operators';

import { ITicker } from '../../models/iticker';
import { DummyService } from '../dummy/dummy.service';
import { TimerService } from '../timerservice/timer.service';

@Injectable({
  providedIn: 'root'
})
export class BinanceService {

  private rootURL = 'https://sheltered-basin-26277.herokuapp.com/';
  private apiURL = 'https://api.binance.com/api/v1/';
  private refresh$ = new Subject<void>();

  constructor(private _http: HttpClient,
    private dummyService: DummyService,
    private timerService: TimerService) {
    this.timerService.getTimer(1000, () => { this.refresh(); }).subscribe();
  }

  private getTickerResponse(symbol: string): Observable<HttpResponse<ITicker>> {
    const __headers = new HttpHeaders();
    const __body: any = null;

    const req = new HttpRequest<any>(
      'GET',
      this.getAPIURL() + '/ticker/24hr?symbol=' + symbol,
      __body,
      {
        headers: __headers,
        responseType: 'json'
      });

    return this._http.request<any>(req).pipe(
      filter(_r => _r instanceof HttpResponse),
      map(_r => {
        const _resp = _r as HttpResponse<any>;
        let _body: ITicker = null;
        _body = _resp.body as ITicker;
        return _resp.clone({ body: _body }) as HttpResponse<ITicker>;
      })
    );
  }

  private getRealTicker(symbol: string) {
    return this.getTickerResponse(symbol).pipe(
      map(_r => _r.body)
    );
  }

  public getTicker(symbol: string): Observable<ITicker> {
    return combineLatest(this.refresh$, this.dummyService.useDummies()).pipe(
      switchMap(([refresh, useDummies]: [void, boolean]) => of(useDummies)),
      exhaustMap((useDummies) => {
        return useDummies ?
          this.dummyService.getDummyFor(symbol) :
          this.getRealTicker(symbol);
      })
    );
  }

  /**
 * @returns string, consisting of the rootURL + the API URL.
 */
  public getAPIURL(): string {
    return this.rootURL + this.apiURL;
  }

  public refresh() {
    this.refresh$.next(null);
  }


}
