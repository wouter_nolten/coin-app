import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { inject, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { IWallet } from 'src/app/models/iwallet';

import { DummyService } from '../dummy/dummy.service';
import { GlobalService } from '../global/global.service';
import { TimerService } from '../timerservice/timer.service';
import { WalletService } from './wallet.service';

const globalService = jasmine.createSpyObj('GlobalService', ['getDefaultCrypto', 'getDefaultCurrency', 'getAcceptedCoins']);
const dummyService = jasmine.createSpyObj('DummyService', ['useDummies', 'getDummyWallet']);
const timerService = jasmine.createSpyObj('TimerService', ['getTimer']);

const stubRealWallet = {
  balances: [{
    name: 'NEO',
    valid: 20,
    total: 20
  },
  {
    name: `GAS`,
    valid: 16,
    total: 16
  }],
  address: ''
} as IWallet;

const stubDummyWallet = {
  balances: [{
    name: 'NEO',
    valid: 15,
    total: 15
  },
  {
    name: `GAS`,
    valid: 16,
    total: 16
  }],
  address: ''
} as IWallet;


describe('WalletService', () => {
  let walletService: WalletService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [WalletService,
        { provide: GlobalService, useValue: globalService },
        { provide: DummyService, useValue: dummyService },
        { provide: TimerService, useValue: timerService }
      ]
    });

    // Inject the http, test controller, and service-under-test
    // as they will be referenced by each test.
    httpTestingController = TestBed.get(HttpTestingController);

    globalService.getAcceptedCoins.and.returnValue(['NEO', 'GAS']);

    dummyService.getDummyWallet.and.returnValue(of(stubDummyWallet));
    dummyService.useDummies.and.returnValue(of(false));
    timerService.getTimer.and.returnValue(of(1)); // only call refresh once.

    walletService = TestBed.get(WalletService);
  });

  afterEach(() => {
    // After every test, assert that there are no more pending requests.
    httpTestingController.verify();
  });

  it('should be created', inject([WalletService], (service: WalletService) => {
    expect(service).toBeTruthy();
  }));

  it('should get wallet with dummies turned off', (done: DoneFn) => {
    walletService.getWallet('NEO', '').subscribe(wallet => {
      expect(wallet).toBeDefined();
      expect(wallet.balances.length).toEqual(2);
      expect(wallet.balances[0].total).toEqual(20);
      done();
    });

    dummyService.useDummies.and.returnValue(of(false));
    walletService.refresh();

    const req = httpTestingController.expectOne(walletService.getAPIURL() + 'NEO');
    expect(req.request.method).toEqual('GET');

    // Respond with the mock wallet
    req.flush(stubRealWallet);
  });

  it('should get wallet with dummies turned on', (done: DoneFn) => {
    dummyService.useDummies.and.returnValue(of(true));

    walletService.getWallet('DUMMY', '').subscribe(wallet => {
      expect(wallet).toBeDefined();
      expect(wallet.balances.length).toEqual(2);
      expect(wallet.balances[0].total).toEqual(15);
      done();
    });

    walletService.refresh();
  });
});
