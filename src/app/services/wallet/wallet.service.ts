import { HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of, Subject } from 'rxjs';
import {
  catchError,
  exhaustMap,
  filter,
  map,
  publishReplay,
  refCount,
  startWith,
  takeUntil,
  tap,
  withLatestFrom,
} from 'rxjs/operators';
import { ICoin } from 'src/app/models/icoin';

import { IWallet } from '../../models/iwallet';
import { DummyService } from '../dummy/dummy.service';
import { GlobalService } from '../global/global.service';
import { TimerService } from '../timerservice/timer.service';

@Injectable({
  providedIn: 'root'
})
export class WalletService {
  private rootURL = 'https://sheltered-basin-26277.herokuapp.com/';
  private apiURL = 'https://otcgo.cn/api/v1/balances/';
  private refresh$ = new Subject<void>();
  private wallet$: Observable<IWallet>;
  private destroy$ = new Subject<void>();
  private isLoading$ = new Subject<boolean>();

  constructor(private _http: HttpClient,
    private dummyService: DummyService,
    private timerService: TimerService,
    private globalService: GlobalService) {
    this.timerService.getTimer(1000, () => { this.refresh(); }).subscribe();
  }

  /**
   * returns a new wallet.
   * @param symbol: which symbol to use (i.e. NEO/USDT)
   * @param sessionId: current session ID.
   */
  public getWallet(symbol: string, sessionId: string): Observable<IWallet> {
    this.setLoading(true);

    this.wallet$ = this.refresh$.pipe(
      takeUntil(this.destroy$),
      withLatestFrom(this.dummyService.useDummies()),
      exhaustMap(([refresh, useDummies]: [void, boolean]) => {
        return useDummies ?
          this.dummyService.getDummyWallet(symbol) :
          this.getRealWallet(symbol, '');
      }),
      filter(wallet => !!wallet),
      map((wallet: IWallet) => {
        wallet.balances = wallet.balances.filter((coin: ICoin) => this.filterAcceptedCoin(coin.name));
        return wallet;
      }),
      tap(() => this.setLoading(false)),
      publishReplay(1),
      refCount()
    );

    return this.wallet$;
  }

  public refresh() {
    this.refresh$.next(null);
  }

  public clearWallet() {
    if (this.wallet$) {
      this.destroy$.next(null);
    }
  }

  public isLoading(): Observable<boolean> {
    return this.isLoading$.pipe(
      startWith(false),
      map((loading) => loading)
    );
  }

  /**
   * @returns string, consisting of the rootURL + the API URL.
   */
  public getAPIURL(): string {
    return this.rootURL + this.apiURL;
  }

  /**
  * getWalletResponse from Wallet server.
  * @returns Observable<HttpResponse<Wallet>>
  */
  private getWalletResponse(symbol: string, sessionId: string): Observable<HttpResponse<IWallet>> {
    const __headers = new HttpHeaders();
    const __body: any = null;

    const req = new HttpRequest<any>(
      'GET',
      this.getAPIURL() + symbol,
      __body,
      {
        headers: __headers,
        responseType: 'json'
      }
    );

    return this._http.request<any>(req).pipe(
      filter(_r => _r instanceof HttpResponse),
      map(_r => {
        const _resp = _r as HttpResponse<any>;
        return _resp.clone({ body: _resp.body }) as HttpResponse<IWallet>;
      })
    );
  }

  /**
  * getWallet from server.
  * @return Observable<Wallet>
  */
  private getRealWallet(symbol: string, sessionId: string): Observable<IWallet> {
    return this.getWalletResponse(symbol, sessionId).pipe(
      map(_r => _r.body),
      catchError((error) => {
        console.log('Error response: ', error);
        this.isLoading$.next(false);
        return of(null);
      })
    );
  }

  private filterAcceptedCoin(coinName: string) {
    return this.globalService.getAcceptedCoins().indexOf(coinName) !== -1;
  }

  private setLoading(loading: boolean) {
    this.isLoading$.next(loading);
  }
}
