import { ITicker } from '../../models/iticker';

export class GlobalService {
  private readonly _INVALID_TICKER = {
    askPrice: 0,
    askQty: 0,
    bidPrice: 0,
    bidQty: 0,
    closeTime: 0,
    count: 0,
    firstId: 0,
    highPrice: 0,
    lastId: 0,
    lastPrice: 0,
    lastQty: 0,
    lowPrice: 0,
    openPrice: 0,
    openTime: 0,
    prevClosePrice: 0,
    priceChange: 0,
    priceChangePercent: 0,
    quoteVolume: 0,
    symbol: 'INVALIDTICKER',
    volume: 0,
    weightedAvgPrice: 0,
  } as ITicker;

  public getDefaultCurrency(): string {
    return 'USDT';
  }

  public getDefaultCrypto(): string {
    return 'NEO';
  }

  public getAvailableTickers(): string[] {
    return ['NEO', 'GAS'];
  }

  public getDefaultTickerTime(): number {
    return 1000;
  }

  public getInvalidTicker(): ITicker {
    return this._INVALID_TICKER;
  }

  public getAcceptedCoins(): string[] {
    return this.getAvailableTickers();
  }
}
