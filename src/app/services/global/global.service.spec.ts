import { TestBed, inject } from '@angular/core/testing';

import { GlobalService } from './global.service';

describe('GlobalService', () => {
    let globalService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [GlobalService]
        });

        globalService = TestBed.get(GlobalService);
    });

    it('should be created', inject([GlobalService], (service: GlobalService) => {
        expect(service).toBeTruthy();
    }));

    it('should have a default currency', () => {
        const defCurrency = globalService.getDefaultCurrency();
        expect(defCurrency).toBeDefined();
        expect(defCurrency).not.toEqual('');
    });

    it('should have a default for available tickers', () => {
        const defTickers = globalService.getAvailableTickers();
        expect(defTickers).toBeDefined();
        expect(defTickers.length).toBeGreaterThan(0);
    });

    it('should have a default for ticker time', () => {
        const defTickerTime = globalService.getDefaultTickerTime();
        expect(defTickerTime).toBeDefined();
        expect(defTickerTime).toBeGreaterThan(0);
    });

    it('should have a default invalid ticker', () => {
        const defInvalidTicker = globalService.getInvalidTicker();
        expect(defInvalidTicker).toBeDefined();
        expect(defInvalidTicker.symbol).toEqual('INVALIDTICKER');
    });
});
