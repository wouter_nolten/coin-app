import { Injectable } from '@angular/core';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class DcaService {

  private readonly dcaFrequencies: string[] = [
    'day',
    'week',
    'month',
    'year'
  ];

  private readonly MOMENT_ONE = '1' as moment.unitOfTime.DurationConstructor;

  private currentDCAFrequency = this.dcaFrequencies[0];
  private dcaAmount = 0;
  private nextDCADate: moment.Moment;

  constructor() {
    this.calculateNextDCADate(this.currentDCAFrequency);
  }

  /**
   * @returns number current DCA Amount.
   */
  public getDCAAmount(): number {
    return this.dcaAmount;
  }

  /**
   * @returns moment.Moment: next DCA date.
   */
  public getNextDCADate(): moment.Moment {
    return this.nextDCADate;
  }

  /**
   * Sets the current amount to DCA to a certain amount.
   * @param amount amount to set the DCA amount to.
   */
  public setDCAAmount(amount: number) {
    this.dcaAmount = Math.abs(amount);
  }

  /**
   * @returns current frequency which a DCA will be executed (days | weeks | months | year)
   */
  public getCurrentDCAFrequency(): string {
    return this.currentDCAFrequency;
  }

  /**
   * @returns a list in which a DCA can be executed (days | weeks | months | year)
   */
  public getDCAFrequencies(): string[] {
    return this.dcaFrequencies;
  }

  /**
   * Updates the DCA frequency.
   * @param newFrequency string: new frequency to update DCA with, every: day | week | month | year
   */
  public updateDCAFrequency(newFrequency: string) {
    this.currentDCAFrequency = newFrequency;
    this.calculateNextDCADate(newFrequency);
  }

  /**
   * calculate the next date based on the frequency of DCA
   * @param newFrequency string: new frequency to update DCA with, every: day | week | month | year
   */
  private calculateNextDCADate(newFrequency: string) {
    if (this.dcaFrequencies.indexOf(newFrequency) === -1) {
      throw new Error(`Mismatch in new DCA date: ${newFrequency}. Allowed are day | week | month | year`);
    }
    this.nextDCADate = moment().add(this.MOMENT_ONE, `${newFrequency}s`);
  }
}
