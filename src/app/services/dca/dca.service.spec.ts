import { inject, TestBed } from '@angular/core/testing';
import * as moment from 'moment';

import { DcaService } from './dca.service';

const dcaFrequencies: string[] = [
  'day',
  'week',
  'month',
  'year'
];

const tomorrow = moment().add('1', 'days').format('DD/MM/YYYY');
const nextMonth = moment().add('1', 'months').format('DD/MM/YYYY');

describe('DcaService', () => {
  let dcaService: DcaService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DcaService]
    });

    dcaService = TestBed.get(DcaService);
  });

  it('should be created', inject([DcaService], (service: DcaService) => {
    expect(service).toBeTruthy();
  }));

  it('should test getters', () => {
    const amount = dcaService.getDCAAmount();
    expect(amount).toBeDefined();

    const nextDCADate = dcaService.getNextDCADate();
    expect(nextDCADate).toBeDefined();
  });

  it('should be able to set a positive DCA amount', () => {
    dcaService.setDCAAmount(5);
    expect(dcaService.getDCAAmount()).toEqual(5);

    dcaService.setDCAAmount(1);
    expect(dcaService.getDCAAmount()).toEqual(1);
  });

  it('should always set the absolute dca amount', () => {
    dcaService.setDCAAmount(-2);
    expect(dcaService.getDCAAmount()).toEqual(2);
  });

  it('should display a list of usable frequencies', () => {
    const frequencies = dcaService.getDCAFrequencies();
    expect(frequencies).toEqual(dcaFrequencies);
  });

  it('should be able to update the DCA frequency', () => {
    dcaService.updateDCAFrequency(dcaFrequencies[0]);
    expect(dcaService.getCurrentDCAFrequency()).toEqual(dcaFrequencies[0]);
    expect(dcaService.getNextDCADate().format('DD/MM/YYYY')).toEqual(tomorrow);

    dcaService.updateDCAFrequency(dcaFrequencies[2]);
    expect(dcaService.getCurrentDCAFrequency()).toEqual(dcaFrequencies[2]);
    expect(dcaService.getNextDCADate().format('DD/MM/YYYY')).toEqual(nextMonth);
  });
});
