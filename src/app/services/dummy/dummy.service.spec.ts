/* tslint:disable:no-unused-variable */
import { inject, TestBed } from '@angular/core/testing';
import { skip } from 'rxjs/operators';

import { DummyService } from './dummy.service';


describe('DummyService', () => {
  let dummyService: DummyService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DummyService]
    });

    dummyService = TestBed.get(DummyService);
  });

  it('should create', inject([DummyService], (service: DummyService) => {
    expect(service).toBeTruthy();
  }));

  it('should have a usedummies observable which is defaulted to false', (done: DoneFn) => {
    dummyService.useDummies().subscribe(value => {
      expect(value).toBe(false);
      done();
    });
  });

  it('should return a dummy wallet with two coins', (done: DoneFn) => {
    dummyService.getDummyWallet('NEO').subscribe(wallet => {
      expect(wallet.balances.length).toEqual(2);
      expect(wallet.balances[0].name).toEqual('NEO');
      done();
    });
  });

  it('should return a dummy ticker', (done: DoneFn) => {
    dummyService.getDummyFor('NEOUSDT').subscribe(ticker => {
      expect(ticker.symbol).toEqual('NEOUSDT');
      done();
    });
  });

  it('should return error for invalid symbol', (done: DoneFn) => {
    dummyService.getDummyFor('INVALIDTOKEN').subscribe(
      () => { },
      (err) => {
        expect(err.error.error).toEqual('No such coin');
        done();
      }
    );
  });

  it('should be able to change the dummy observable', (done: DoneFn) => {
    dummyService.useDummies()
      .pipe(skip(1))
      .subscribe(value => {
        expect(value).toBe(true);
        done();
      });

    dummyService.setDummies(true);
  });

});
