import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of, throwError } from 'rxjs';

import { Dummy } from '../../models/dummy/dummy';
import { IWallet } from '../../models/iwallet';
import { ITicker } from '../../models/iticker';

@Injectable({
  providedIn: 'root'
})
export class DummyService {

  private dummy = new Dummy();
  private setDummies$ = new BehaviorSubject<boolean>(false);

  constructor() {
  }

  public setDummies(newValue: boolean) {
    this.setDummies$.next(newValue);
  }

  public useDummies(): BehaviorSubject<boolean> {
    return this.setDummies$;
  }

  public getDummyWallet(currency: string): Observable<IWallet> {
    return of(this.dummy.DUMMY_WALLET);
  }

  public getDummyFor(dummy: string): Observable<ITicker> {
    let ticker;
    switch (dummy) {
      case 'NEOUSDT':
        ticker = this.dummy.DUMMY_TICKER_NEOUSDT;
        break;
      case 'GASBTC':
        ticker = this.dummy.DUMMY_TICKER_GASBTC;
        break;
      case 'BTCUSDT':
        ticker = this.dummy.DUMMY_TICKER_BTCUSDT;
        break;
      default:
        return throwError({
          error: {
            code: -1121,
            error: 'No such coin'
          }
        });
    }
    return of(ticker);
  }

}
