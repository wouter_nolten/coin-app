import { Injectable } from '@angular/core';
import { timer, of, Observable } from 'rxjs';
import { exhaustMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TimerService {

  constructor() { }

  /**
   *  getTimer: creates a new observable which emits every x seconds and calls the callback.
   * @param milliSeconds
   * @param callback
   */
  public getTimer(milliSeconds, callback): Observable<number> {
    return timer(0, milliSeconds).pipe(
      exhaustMap(() => of(callback()))
    );
  }
}
