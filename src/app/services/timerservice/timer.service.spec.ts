/* tslint:disable:no-unused-variable */
import { inject, TestBed } from '@angular/core/testing';

import { TimerService } from './timer.service';


describe('TimerService', () => {
  let timerService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TimerService]
    });

    timerService = TestBed.get(TimerService);
  });

  it('should create', inject([TimerService], (service: TimerService) => {
    expect(service).toBeTruthy();
    expect(timerService).toBeTruthy();
  }));

  it('should be able to make a timer', (done: DoneFn) => {
    const time = 100;
    const foo = {
      bar: () => { }
    };

    spyOn(foo, 'bar');

    timerService.getTimer(time, foo.bar).subscribe();

    setTimeout(() => {
      expect(foo.bar).toHaveBeenCalledTimes(2);
      done();
    }, time + 10);
  });
});
